# Step to initatiate the scan

## Create token
```
curl -s -k -X POST -H 'Content-Type: application/json' -d "{\"username\":\"yourusername\",\"password\":\"yourpassword\"}" https://localhost:8834/session

curl -s -k -X POST -H 'Content-Type: application/json' -d "{\"username\":\"yourusername\",\"password\":\"$cat /path/your/password"}" https://localhost:8834/session

curl -s -k -X POST -H 'Content-Type: application/json' -d -u admin:$cat /path/your/password https://localhost:8834/session
```

## List folder
```
curl -s -k -X GET -H "X-Cookie: token=2edbec9ae830eb33996c5d07329d3bba21be00773f690cd9" https://localhost:8834/folders | python3 -m json.tool
```

## Create folder
```
curl -s -k -X POST -H "X-Cookie: token=2edbec9ae830eb33996c5d07329d3bba21be00773f690cd9" -H 'Content-Type: application/json' -d '{"name": "api_folder"}' https://localhost:8834/folders
```

## List policies
```
curl -s -k -X GET -H "X-Cookie: token=2edbec9ae830eb33996c5d07329d3bba21be00773f690cd9" https://localhost:8834/policies | python3 -m json.tool | python3 -m json.tool
```

## Create Scan
```
curl -s -k -X POST -H "X-Cookie: token=2edbec9ae830eb33996c5d07329d3bba21be00773f690cd9" -H 'Content-Type: application/json' -d '{"uuid": "bbd4f805-3966-d464-b2d1-0079eb89d69708c3a05ec2812bcf", "settings": { "name": "scan_name", "text_targets": "TARGETIP", "policy_id": "111", "folder_id":"110" } }' https://localhost:8834/scans | python3 -m json.tool
```

## Start Scan
```
curl -s -k -X POST -H "X-Cookie: token=2edbec9ae830eb33996c5d07329d3bba21be00773f690cd9" -d '' https://localhost:8834/scans/113/launch
```

## View result
```
curl -s -k -X GET -H "X-Cookie: token=2edbec9ae830eb33996c5d07329d3bba21be00773f690cd9" -d '' https://localhost:8834/scans/113/ | python3 -m json.tool
```

## Check file status
```
curl -s -k -X GET -H "X-Cookie: token=2edbec9ae830eb33996c5d07329d3bba21be00773f690cd9" -H 'Content-Type: application/json' https://localhost:8834/scans/318/export/1178397097/status

curl -s -k -X GET -H "X-Cookie: token=2edbec9ae830eb33996c5d07329d3bba21be00773f690cd9" -d '' https://localhost:8834/scans/113/hosts/61438 | python3 -m json.tool
```
